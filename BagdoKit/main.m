//
//  main.m
//  BagdoKit
//
//  Created by Magno Cardona on 06-12-12.
//  Copyright (c) 2012 Magno Cardona. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
