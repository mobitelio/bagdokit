//
//  AppDelegate+Keychain.h
//  ErrazurizWineries
//
//  Created by Magno Cardona on 05-12-12.
//  Copyright (c) 2012 Francisco MuÃ±oz. All rights reserved.
//

//  This class will add 
#import "BKAppDelegate.h"
#import "KeychainItemWrapper.h"

@interface BKAppDelegate (Keychain)

@property (strong, nonatomic) KeychainItemWrapper *keychain;

#pragma mark - KeyChain
- (void) initKeychain;

- (NSMutableDictionary*) getKeychainDataDelegate;
- (NSString*) getKeychainPassword;
- (NSString*) getKeychainUser;
- (id) getKeychainValue: (NSString*) forKey;
- (void) setKeychainDataWithKey: (NSString*) key  forObject:(id) forObject;

@end
