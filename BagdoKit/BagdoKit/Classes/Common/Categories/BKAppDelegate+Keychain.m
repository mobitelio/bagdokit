//
//  AppDelegate+Keychain.m
//  ErrazurizWineries
//
//  Created by Magno Cardona on 05-12-12.
//  Copyright (c) 2012 Francisco MuÃ±oz. All rights reserved.
//

#import "BKAppDelegate+Keychain.h"
#import "MKNetworkKit.h"
#import "KeychainItemWrapper.h"
#import <objc/runtime.h>

static char const * const keychainKey = "keychainKey";

@implementation BKAppDelegate (Keychain)

@dynamic keychain;


#pragma mark - Getters / Setters

- (id)keychain {
    return objc_getAssociatedObject(self, keychainKey);
}

- (void)setKeychain:(id)newObj {
    objc_setAssociatedObject(self, keychainKey, newObj, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}


#pragma mark - KeyChain

//keychain stores the user, password, and data for the delegate
- (void) initKeychain
{
    // Create instance of keychain wrapper
	[self setKeychain:[[KeychainItemWrapper alloc] initWithIdentifier:@"KeychainErrazuriz" accessGroup:nil]];
}

//Get The keychain data is store as a nsstring json, we trasform the string into a dictionary.
- (NSMutableDictionary *) getKeychainDataDelegate
{
    
    NSString *str = [[self keychain] objectForKey:(__bridge id)kSecAttrService];
    
    if (str!=nil) {
        
        NSData* data=[str dataUsingEncoding:NSUTF8StringEncoding];
        
        NSError* error = nil;
        
        NSMutableDictionary* json = [NSJSONSerialization
                                     JSONObjectWithData:data
                                     options:kNilOptions
                                     error:&error];
        
        if (error == nil && json != nil) {
            
            return [[NSMutableDictionary alloc] initWithDictionary:json copyItems:YES];
            
        }
    }
    
    //something went wrong returning empty dic
    return [[NSMutableDictionary alloc] init];
    
}


//Get user keychain
- (NSString *) getKeychainUser
{
    
    NSString *username = [[self keychain] objectForKey:(__bridge id)kSecAttrAccount];
    return username;
    
}


//Get Password Keychain
- (NSString *) getKeychainPassword
{
    
    NSString *password2 = [[self keychain] objectForKey:(__bridge id)kSecValueData];
    return password2;
    
}

//Set Keychain Data
- (void) setKeychainDataDelegate: (NSMutableDictionary *) data
{
    
    NSString *json =  [data jsonEncodedKeyValueString]; //jsonkit must be installed
    [[self keychain] setObject:json forKey: (__bridge id)kSecAttrService];
    
}


//Set User Keychain
- (void) getKeychainUser: (NSString *) username
{
    
    [[self keychain] setObject:username forKey:(__bridge id)kSecAttrAccount];
    
}


//Set Password Keychain
- (void) getKeychainPassword: (NSString *) password2
{
    
    [[self keychain] setObject:password2 forKey:(__bridge id)kSecValueData];
    
}

//get
- (id) getKeychainValue: (NSString*) forKey
{
    
    NSMutableDictionary *data = [self getKeychainDataDelegate];
    return [data objectForKey: forKey];
    
}

//set objects for the keychain
- (void) setKeychainDataWithKey: (NSString*) key  forObject:(id) forObject
{
    
    NSMutableDictionary *data = [self getKeychainDataDelegate];
    
    if (forObject==nil) {
        [data removeObjectForKey:key];
    } else {
        [data setObject:forObject forKey: key];
    }
    
    [self setKeychainDataDelegate:data];
    
}


@end
