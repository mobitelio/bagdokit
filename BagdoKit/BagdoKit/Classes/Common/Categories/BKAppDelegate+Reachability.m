//
//  AppDelegate+Reachability.m
//  ErrazurizWineries
//
//  Created by Magno Cardona on 05-12-12.
//  Copyright (c) 2012 Francisco MuÃ±oz. All rights reserved.
//

#import "BKAppDelegate+Reachability.h"
#import <objc/runtime.h>


static char const * const timer = "timer";
static char const * const reachability = "reachability";

@implementation BKAppDelegate (Reachability)

@dynamic timer;
@dynamic reachability;


#pragma mark - Getters / Setters
- (id)timer {
    return objc_getAssociatedObject(self, timer);
}

- (void)setTimer:(id)newObj {
    objc_setAssociatedObject(self, timer, newObj, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}


- (id)reachability {
    return objc_getAssociatedObject(self, reachability);
}

- (void)setReachability:(id)newObj {
    objc_setAssociatedObject(self, reachability, newObj, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}


#pragma mark - Reachability


- (void) initReachabilityWithHost: (NSString*) hostUrl {
    
    [self  haltReachability];
    
    [self setReachability:[Reachability reachabilityWithHostname:hostUrl]];
    
    [self setTimer: [NSTimer scheduledTimerWithTimeInterval:60.0f
                                                     target:self
                                                   selector:@selector(logReachability)
                                                   userInfo:[self timer]
                                                    repeats:YES]];
    return;
}

- (void) haltReachability
{
    
    //Stop timer, till changes are done
    if ([self timer] != nil) {
        [[self timer] invalidate];
        [self setTimer: nil];
    }
    
    return;
}



-(void) logReachability {
    NSLog(@"Reachability WIFI: %d | WWAN %d", [self reachability].isReachableViaWiFi, [self reachability].isReachableViaWWAN);
}

-(BOOL) isReachable {
    return [[self reachability] isReachable];
}

-(BOOL) isReachableViaWiFi {
    return [[self reachability] isReachableViaWiFi];
}

-(BOOL) isReachableViaWWAN {
    return [[self reachability] isReachableViaWWAN];
}

@end
