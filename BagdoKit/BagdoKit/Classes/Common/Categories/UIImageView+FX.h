//
//  UIImageView+FX.h
//  Whosapp
//
//  Created by Daniel Gutiérrez Torres on 30-06-12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "CALayer+LayerWithName.h"

@interface UIImageView (FX)

-(void)roundedCorners:(int)round;
-(void)bordersWithColor:(UIColor *)color withWidth:(int)width;
-(void)shadowWithColor:(UIColor *)color withOpacity:(float)opacity withRadius:(int)radius withOffsetX:(int)offsetX withOffsetY:(int)offsetY;



@end
