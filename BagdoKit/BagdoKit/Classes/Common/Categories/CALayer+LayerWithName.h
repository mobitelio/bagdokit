//
//  CALayer+LayerWithName.h
//  Whosapp
//
//  Created by Daniel Gutiérrez Torres on 30-06-12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

@interface CALayer (LayerWithName)

-(CALayer *)layerWithName:(NSString *)name;

@end
