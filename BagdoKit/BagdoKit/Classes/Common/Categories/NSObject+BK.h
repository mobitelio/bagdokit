//
//  NSObject+BK.h
//  BagdoKit
//
//  Created by Magno Cardona on 23-01-13.
//  Copyright (c) 2013 Magno Cardona. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (BK)


/**
 Finds an element In an array of dictionary
 
 @param array of dictionaries
 @param forKey key to find
 @param withKeyValue value of the key to match
 @param andReturnThisKeyValue key to return
 @return NSObject of the the key
 @since 0.1
 */
- (id) findElementInArrayOfDictionary:(NSArray*)array
                               forKey:(NSString*)key
                         withKeyValue:(NSString*)valueKey
                andReturnThisKeyValue: (NSString*)keyReturn;


/**
 Finds the index in array of the element in an array of dictionary
 
 @param array of dictionaries
 @param forKey key to find
 @param withKeyValue value of the key to match
 @return index of element
 @since 0.1
 */
- (int) findIndexInArrayOfDictionary:(NSArray*)array
                              forKey:(NSString*)key
                        withKeyValue:(NSString*)valueKey;

- (BOOL)isRetina;

- (BOOL)isIOS6;
@end
