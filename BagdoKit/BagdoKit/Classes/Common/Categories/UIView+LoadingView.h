//
//  UIView+LoadingView.h
//
//  Created by Luis Mancilla & Magno Cardona on 8/23/12.
//  Copyright (c) 2012 Mobitelio. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 This category adds an UIView with a label and activityspinner to the actual UIView
 Loading Mask Can Be Customize (Background Color, Opacity, Text, etc)
 Handle Asyncronic stoping use locks to setup the counter.
 
 @author Magno Cardona & Luis Mancilla
 @version 0.1
 */
@interface UIView (LoadingView)


typedef void (^UIViewLoadingViewBlock)();


#pragma mark - Constructor Methods


/**
 Adds convinience method to add a subview of the loading mask
 @return UIView loading mask
 @since 0.1
 */
- (UIView *) loadingStart;
        

/**
 Adds convinience method to add a subview of the loading mask, with a custom text
 @param text NSString for the loading mask
 @return UIView loading mask
 @since 0.1
 */
- (UIView *) loadingStartWithText:(NSString *)text;


/**
 Adds convinience method to add a subview of the loading mask with custom text, and stop counter.
 
 Note: if the totalLocks = 2, means you can stop the loading mask twice before disapearing the loader mask.
 This is usefull when doing asyncronic networks calls.
 @param text NSString for the loading mask
 @param counter NSInteger for the loading mask
 @return UIView loading mask
 @since 0.1
 */
- (UIView *) loadingStartWithText:(NSString *)text withTotalLocks:(NSInteger)totalLocks;


/**
 Adds convinience method to remove the loading mask. If using totalLocks loading mask gets removes if counter reaches 0.
 @since 0.1
 */
- (void) loadingStop;


/**
 Adds convinience method to remove the loading mask even if totalLocks is bigger than 0.
 @since 0.1
 */
- (void) loadingHalt;



#pragma mark - Extra Setting Methods

/**
 Adds convinience method to setup the loading mask counter
 @param NSNumber counter for the loadkingmask
 @since 0.1
 */
- (void) setLoadingViewTotalLocks:(NSNumber *)withTotalLocks;


/**
 Adds convinience method to setup backgorund of the loading mask
 @param UIColor color
 @since 0.1
 */
- (void) setLoadingViewBackgroundColor:(UIColor *)withColor;


/**
 Adds convinience method to chnage the loading mask text.
 Note: Uses NSLocalizedString to find i18 text
 @param NSString label text
 @since 0.1
 */
- (void) setLoadingViewLabelText:(NSString *)withText;

/**
 Adds convinience method to increment the loading mask border radious 
 @param CGFloat radius
 @since 0.1
 */
- (void) setLoadingViewBorderRadius:(CGFloat)withRadius;


/**
 Adds convinience method to define the loading mask opacity
 @param CGFloat opacity (range 0.0f - 1.0f  defualt:.8f)
 @since 0.1
 */
- (void) setLoadingViewOpacity:(CGFloat)withOpacity;


/**
 Adds convinience method to define the loading mask padding 
 Note: Paddin between the uiview and the loading mask
 @param CGFloat padding
 @since 0.1
 */
- (void) setLoadingViewPadding:(CGFloat)withPadding;


/**
 Adds convinience method to set up a target and selector which is called
 after stopping the loadingView mask. (example loadingViewDidFinish)
 Note: Paddin between the uiview and the loading mask
 @param id target (could be self)
 @param id selector (a method that must exist in the target)
 @since 0.1
 */
- (void) setLoadingViewBlock:(UIViewLoadingViewBlock) completionBlock;
@end
