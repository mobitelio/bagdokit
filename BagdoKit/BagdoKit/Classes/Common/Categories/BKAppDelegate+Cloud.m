//
//  AppDelegate+Cloud.m
//  ErrazurizWineries
//
//  Created by Magno Cardona on 05-12-12.
//  Copyright (c) 2012 Francisco MuÃ±oz. All rights reserved.
//

#import "BKAppDelegate+Cloud.h"




@implementation BKAppDelegate (Cloud)

-(void) disableiCloud {
    
    NSFileManager *filemgr;
    NSArray *dirPaths;
    NSString *docsDir;
    
    filemgr =[NSFileManager defaultManager];
    
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                   NSUserDomainMask, YES);
    docsDir = [dirPaths objectAtIndex:0];
    
    
    [self addSkipBackupAttributeToItemAtURL:[NSURL fileURLWithPath:docsDir]];
    
}

- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL
{
    assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
    
    NSError *error = nil;
    BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                                  forKey: NSURLIsExcludedFromBackupKey error: &error];
    if(!success){
        NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
    }
    return success;
}

@end
