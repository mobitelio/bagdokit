//
//  UIWebView+removeShadow.m
//  ErrazurizWineries
//
//  Created by Daniel Gutierrez on 9/21/12.
//  Copyright (c) 2012 Francisco MuÃ±oz. All rights reserved.
//

#import "UIWebView+removeShadow.h"


@implementation UIWebView (removeShadow)


-(void)removeShadow{
	[self setOpaque:NO];
    for(UIView *wview in [[[self subviews] objectAtIndex:0] subviews]) {
        if([wview isKindOfClass:[UIImageView class]]) { wview.hidden = YES; }
    }
}

@end
