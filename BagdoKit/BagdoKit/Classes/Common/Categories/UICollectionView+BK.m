//
//  UICollectionView+BK.m
//  BagdoKit
//
//  Created by Magno Cardona on 02-01-13.
//  Copyright (c) 2013 Magno Cardona. All rights reserved.
//

#import "UICollectionView+BK.h"

@implementation UICollectionView (BK)

-(NSIndexPath*) indexPathsForSelectedItemsAtIndex: (NSInteger) index
{

    NSArray *indexPathArray = [self indexPathsForSelectedItems];
    
    if ([indexPathArray count] > index) {
        
        NSIndexPath *indexPath = (NSIndexPath *)indexPathArray[index];
        
        return indexPath;
    }

    return nil;
}

@end
