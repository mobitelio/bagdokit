//
//  NSFileManager+BK.h
//  BagdoKit
//
//  Created by Magno Cardona on 19-01-13.
//  Copyright (c) 2013 Magno Cardona. All rights reserved.
//



@interface NSFileManager (BK)


- (NSArray*) arrayWithFilesMatchingPattern: (NSString*) pattern inDirectory: (NSString*) directory;

- (NSArray*) arrayOrderByDate: (NSArray*)filesDated;

@end
