//
//  NSFileManager+BK.m
//  BagdoKit
//
//  Created by Magno Cardona on 19-01-13.
//  Copyright (c) 2013 Magno Cardona. All rights reserved.
//

#import "NSFileManager+BK.h"
#include <glob.h>

@implementation NSFileManager (BK)

typedef enum {
    ASCENDING,
    DESCENGING,
} ArrayFileOrder;

- (NSArray*) arrayWithFilesMatchingPattern: (NSString*) pattern inDirectory: (NSString*) directory {
    
    NSMutableArray* files = [NSMutableArray array];
    glob_t gt;
    NSString* globPathComponent = [NSString stringWithFormat: @"/%@", pattern];
    NSString* expandedDirectory = [directory stringByExpandingTildeInPath];
    const char* fullPattern = [[expandedDirectory stringByAppendingPathComponent: globPathComponent] UTF8String];
    if (glob(fullPattern, 0, NULL, &gt) == 0) {
        int i;
        for (i=0; i<gt.gl_matchc; i++) {
            int len = strlen(gt.gl_pathv[i]);
            NSString* filename = [[NSFileManager defaultManager] stringWithFileSystemRepresentation: gt.gl_pathv[i] length: len];
            [files addObject: filename];
        }
    }
    globfree(&gt);
    return [NSArray arrayWithArray: files];
}


- (NSArray*) arrayOrderByDate: (NSArray*)filesPaths {
    
    NSMutableArray *filesDated = [NSMutableArray array];
    
    
    for (NSString *filePath in filesPaths) {
        
        NSDictionary *prop = [[NSFileManager defaultManager] attributesOfItemAtPath:(NSString *)filePath error:nil];
        NSDate *date = prop[@"NSFileCreationDate"];
        
        [filesDated addObject:@{ @"beginDate" : date, @"filePath" : filePath }];
    }
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"beginDate" ascending:NO];
    [filesDated sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    NSMutableArray *filesPathOrder = [NSMutableArray array];
    
    for (NSDictionary *dic in filesDated) {
        [filesPathOrder addObject:dic[@"filePath"]];
    }
    
    return [NSArray arrayWithArray: filesPathOrder];
}




@end
