//
//  NSString+Helper.h
//  Nevasa
//
//  Created by Magno Cardona on 7/9/11.
//  Copyright 2011 Mobile All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSString (helper)

-(NSString *) reverseString;

-(BOOL) substringExists:(NSString *) key;

-(int) substringExistsAtPosition:(NSString *) key;

- (NSString *)MD5Hash;


- (NSString*)encodeURL;


-(BOOL) checkRut;

- (BOOL)validateEmail;

- (BOOL)validateIsNumber;



@end
