//
//  UICollectionView+BK.h
//  BagdoKit
//
//  Created by Magno Cardona on 02-01-13.
//  Copyright (c) 2013 Magno Cardona. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UICollectionView (BK)

//Gets IndexPath
-(NSIndexPath*) indexPathsForSelectedItemsAtIndex: (NSInteger) index;

@end
