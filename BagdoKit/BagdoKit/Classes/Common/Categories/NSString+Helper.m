//
//  NSString+Helper.m
//  Nevasa
//
//  Created by Magno Cardona on 7/9/11.
//  Copyright 2011 Mobile All rights reserved.
//

#import "NSString+Helper.h"
#import <CommonCrypto/CommonDigest.h>


@implementation NSString (helper)

-(NSString *) reverseString
{
    NSMutableString *reversedStr;
    int len = [self length];
    
    // Auto released string
    reversedStr = [NSMutableString stringWithCapacity:len];     
    
    // Probably woefully inefficient...
    while (len > 0)
        [reversedStr appendString: [NSString stringWithFormat:@"%C", [self characterAtIndex:--len]]];   
    
    return reversedStr;
}


-(BOOL) substringExists: (NSString *) key
{
   
    NSRange match;
    
    match = [self rangeOfString: key];
    
    if (match.location == NSNotFound)
        return NO;
    else
        return YES;

}


-(int) substringExistsAtPosition: (NSString *) key
{
    
    NSRange match;
    
    match = [self rangeOfString: key];
    
    if (match.location == NSNotFound)
        return -1;
    else
        return  match.location;
        
}


- (NSString *)MD5Hash
{
    const char *cStr = [self UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(cStr, strlen(cStr), result);
    
    // Create lowercase NSString
    NSString *format = @"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X";
    NSString *hashedString = [[NSString stringWithFormat:format, result[0], result[1], result[2], result[3], result[4],
                               result[5], result[6], result[7],
                               result[8], result[9], result[10], result[11], result[12],
                               result[13], result[14], result[15]] lowercaseString];
    
    return hashedString;
}


- (NSString*)encodeURL
{
    NSString *string = self;
    NSString *newString = (__bridge NSString *)CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (__bridge CFStringRef)string, NULL, CFSTR(":/?#[]@!$ &'()*+,;=\"<>%{}|\\^~`"), CFStringConvertNSStringEncodingToEncoding(NSUTF8StringEncoding));
    
    if (newString)
    {
        return newString;
    }
    
    return @"";
}


-(BOOL) checkRut {
    
    NSString *rutStr = self;
    
    if ([rutStr length]<2) {
        return NO;
    }
    
    rutStr = [rutStr stringByReplacingOccurrencesOfString:@"." withString:@""];
    rutStr = [rutStr stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
    NSString *lastCharacter = [rutStr substringFromIndex:[rutStr length] - 1];
    lastCharacter = [lastCharacter uppercaseString];
    
    rutStr = [rutStr substringToIndex:[rutStr length] - 1];
    
    NSInteger rut = [rutStr integerValue];
    
    //end with false
    if (rut==0) {
        return NO;
    }
    
    
    NSInteger Digito;
    NSInteger Contador;
    NSInteger Multiplo;
    NSInteger Acumulador;
    
    Contador = 2;
    Acumulador = 0;
    
    while (rut != 0)
    {
        Multiplo = (rut % 10) * Contador;
        Acumulador = Acumulador + Multiplo;
        rut = rut/10;
        Contador = Contador + 1;
        if (Contador == 8)
        {
            Contador = 2;
        }
        
    }
    
    NSString *RutDigito = lastCharacter;
    
    Digito = 11 - (Acumulador % 11);
    
    if (Digito == 10 )
    {
        RutDigito = @"K";
    } else if (Digito == 11)
    {
        RutDigito = @"0";
    } else {
        RutDigito = [NSString stringWithFormat:@"%d", Digito];
    }
    
    
    if ([RutDigito isEqualToString:lastCharacter]) {
        return YES;
    } else {
        return NO;
    }
    
}



- (BOOL)validateEmail {
    
    NSString *inputText = self;
    
    NSString *emailRegex = @"[A-Z0-9a-z][A-Z0-9a-z._%+-]*@[A-Za-z0-9][A-Za-z0-9.-]*\\.[A-Za-z]{2,6}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    NSRange aRange;
    if([emailTest evaluateWithObject:inputText]) {
        aRange = [inputText rangeOfString:@"." options:NSBackwardsSearch range:NSMakeRange(0, [inputText length])];
        int indexOfDot = aRange.location;
        ////NSLog(@"aRange.location:%d - %d",aRange.location, indexOfDot);
        if(aRange.location != NSNotFound) {
            NSString *topLevelDomain = [inputText substringFromIndex:indexOfDot];
            topLevelDomain = [topLevelDomain lowercaseString];
            ////NSLog(@"topleveldomains:%@",topLevelDomain);
            NSSet *TLD;
            TLD = [NSSet setWithObjects:@".aero", @".asia", @".biz", @".cat", @".com", @".coop", @".edu", @".gov", @".info", @".int", @".jobs", @".mil", @".mobi", @".museum", @".name", @".net", @".org", @".pro", @".tel", @".travel", @".ac", @".ad", @".ae", @".af", @".ag", @".ai", @".al", @".am", @".an", @".ao", @".aq", @".ar", @".as", @".at", @".au", @".aw", @".ax", @".az", @".ba", @".bb", @".bd", @".be", @".bf", @".bg", @".bh", @".bi", @".bj", @".bm", @".bn", @".bo", @".br", @".bs", @".bt", @".bv", @".bw", @".by", @".bz", @".ca", @".cc", @".cd", @".cf", @".cg", @".ch", @".ci", @".ck", @".cl", @".cm", @".cn", @".co", @".cr", @".cu", @".cv", @".cx", @".cy", @".cz", @".de", @".dj", @".dk", @".dm", @".do", @".dz", @".ec", @".ee", @".eg", @".er", @".es", @".et", @".eu", @".fi", @".fj", @".fk", @".fm", @".fo", @".fr", @".ga", @".gb", @".gd", @".ge", @".gf", @".gg", @".gh", @".gi", @".gl", @".gm", @".gn", @".gp", @".gq", @".gr", @".gs", @".gt", @".gu", @".gw", @".gy", @".hk", @".hm", @".hn", @".hr", @".ht", @".hu", @".id", @".ie", @" No", @".il", @".im", @".in", @".io", @".iq", @".ir", @".is", @".it", @".je", @".jm", @".jo", @".jp", @".ke", @".kg", @".kh", @".ki", @".km", @".kn", @".kp", @".kr", @".kw", @".ky", @".kz", @".la", @".lb", @".lc", @".li", @".lk", @".lr", @".ls", @".lt", @".lu", @".lv", @".ly", @".ma", @".mc", @".md", @".me", @".mg", @".mh", @".mk", @".ml", @".mm", @".mn", @".mo", @".mp", @".mq", @".mr", @".ms", @".mt", @".mu", @".mv", @".mw", @".mx", @".my", @".mz", @".na", @".nc", @".ne", @".nf", @".ng", @".ni", @".nl", @".no", @".np", @".nr", @".nu", @".nz", @".om", @".pa", @".pe", @".pf", @".pg", @".ph", @".pk", @".pl", @".pm", @".pn", @".pr", @".ps", @".pt", @".pw", @".py", @".qa", @".re", @".ro", @".rs", @".ru", @".rw", @".sa", @".sb", @".sc", @".sd", @".se", @".sg", @".sh", @".si", @".sj", @".sk", @".sl", @".sm", @".sn", @".so", @".sr", @".st", @".su", @".sv", @".sy", @".sz", @".tc", @".td", @".tf", @".tg", @".th", @".tj", @".tk", @".tl", @".tm", @".tn", @".to", @".tp", @".tr", @".tt", @".tv", @".tw", @".tz", @".ua", @".ug", @".uk", @".us", @".uy", @".uz", @".va", @".vc", @".ve", @".vg", @".vi", @".vn", @".vu", @".wf", @".ws", @".ye", @".yt", @".za", @".zm", @".zw", nil];
            if(topLevelDomain != nil && ([TLD containsObject:topLevelDomain])) {
                ////NSLog(@"TLD contains topLevelDomain:%@",topLevelDomain);
                return TRUE;
            }
            /*else {
             //NSLog(@"TLD DOEST NOT contains topLevelDomain:%@",topLevelDomain);
             }*/
            
        }
    }
    return FALSE;
}


- (BOOL) validateIsNumber {
    
    BOOL valid = NO;
    
    NSCharacterSet *alphaNums = [NSCharacterSet decimalDigitCharacterSet];
    
    NSCharacterSet *inStringSet = [NSCharacterSet characterSetWithCharactersInString:self];
    
    valid = [alphaNums isSupersetOfSet:inStringSet];
    
    return valid;
}




@end
