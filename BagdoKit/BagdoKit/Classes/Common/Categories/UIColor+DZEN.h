
//  Created by Ignacio Romero Zurbuchen on 4/24/12.
//  Copyright (c) 2012 DZen Interaktiv. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (DZEN)

+ (UIColor *)colorWithHexString:(NSString *)hexString;

//First 6 symbols like HTML color, latest 2 symbols it’s alpha.
//[UIColor colorWithHex:0xffff0000]; // red
//[UIColor colorWithHex:0x770000FF]; // half-transparent blue
+ (UIColor *) colorWithHex:(uint) hex;

@end
