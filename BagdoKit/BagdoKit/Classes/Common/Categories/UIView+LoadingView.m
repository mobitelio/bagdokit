
//
//  UIView+LoadingView.m
//
//  Created by Luis Mancilla on 8/23/12.
//  Copyright (c) 2012 Mobitelio. All rights reserved.
//

#import "UIView+LoadingView.h"

#import <QuartzCore/QuartzCore.h>
#import <objc/runtime.h>



static void * const kLoadingView = (void*)&kLoadingView;
static void * const kLoadingViewOpacity = (void*)&kLoadingViewOpacity;
static void * const kLoadingViewTotalLocks = (void*)&kLoadingViewTotalLocks;
static void * const kLoadingViewBackgroundColor = (void*)&kLoadingViewBackgroundColor;
static void * const kLoadingViewBorderRadius = (void*)&kLoadingViewBorderRadius;
static void * const kLoadingViewLabelText = (void*)&kLoadingViewLabelText;
static void * const kLoadingViewPadding = (void*)&kLoadingViewPadding;
static void * const kLoadingViewBlock = (void*)&kLoadingViewBlock;


@implementation UIView (LoadingView)

    
#pragma mark - Constructor Methods

- (UIView *) loadingStartWithText:(NSString *)text withTotalLocks:(NSInteger)totalLocks
{
    
    [self setLoadingViewLabelText:text];
    [self setLoadingViewTotalLocks:[NSNumber numberWithInteger:totalLocks]];
    
    return [self loadingStart];
}


- (UIView *) loadingStartWithText:(NSString *)text
{
    
    [self setLoadingViewLabelText:text];
    
    return [self loadingStart];
}


- (UIView *) loadingStart
{
    NSNumber *totalLocks = [self loadingViewTotalLocks];
    
    if (!totalLocks || [totalLocks intValue] == 0) {
        totalLocks = [NSNumber numberWithInt:1];
        [self setLoadingViewTotalLocks:totalLocks];
    }
    
    //Settings
    UIColor *backgroundColor = [self loadingViewBackgroundColor];
    CGFloat borderRadius= [self loadingViewBorderRadius];
    CGFloat contentOpacity = [self loadingViewOpacity];
    CGFloat contentPadding = [self loadingViewPadding];
    NSString *textLabel = [self loadingViewLabelText];
    
    if (!backgroundColor) {
        backgroundColor = [UIColor blackColor];
    }
    
    if (borderRadius == 0.0f) {
        borderRadius = 3.0f;
    }
    
    if (contentOpacity == 0.0f) {
        contentOpacity = .8f;
    }
    
    if (contentPadding == 0.0f) {
        contentPadding = 5.0f;
    }
    
    if (!textLabel || [@""  isEqualToString:textLabel]) {
        textLabel = NSLocalizedString(@"loading ...", nil);
    }
    
    
    UIView *loadingView = [[UIView alloc] initWithFrame:self.bounds];
    loadingView.autoresizingMask = ~UIViewAutoresizingNone;
    loadingView.backgroundColor = [UIColor clearColor];
    loadingView.alpha = 0.0f;
    
    UIView *container = [[UIView alloc] initWithFrame:CGRectInset(loadingView.frame,
                                                                  contentPadding, contentPadding)];
    container.backgroundColor = backgroundColor;
    container.userInteractionEnabled = NO;
    container.alpha = contentOpacity;
    container.autoresizingMask = ~UIViewAutoresizingNone;
    if (borderRadius > 0) {
        [[container layer] setCornerRadius:borderRadius];
        [[container layer] setMasksToBounds:NO];
    }
    
    CGFloat containerWidth = container.frame.size.width;
    
    UIView *content = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, containerWidth, 80)];
    content.autoresizingMask = ~UIViewAutoresizingNone;
    content.backgroundColor = [UIColor clearColor];

    
    loadingView.opaque = NO;
    container.opaque = NO;
    content.opaque = NO;
    
    [content setCenter:CGPointMake(container.frame.size.width/2, container.frame.size.height/2)];
    
    UIActivityIndicatorView *activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    activityIndicatorView.backgroundColor = [UIColor clearColor];
    [activityIndicatorView setCenter:CGPointMake(content.frame.size.width/2, content.frame.size.height/2)];
    CGRect aFrame = activityIndicatorView.frame;
    aFrame.origin.y += 20.0f; //move activity down.
    activityIndicatorView.frame = aFrame;
    activityIndicatorView.autoresizingMask = ~UIViewAutoresizingNone;
    [activityIndicatorView startAnimating];
    
    [content addSubview:activityIndicatorView];
    
    // apply label if view width > DEFAULT_LABEL_WIDTH
    if (self.frame.size.width > content.frame.size.width) {
        // adding label text "loading ..."
        CGRect labelFrame = CGRectMake(0.0f, 0.0f,
                                       content.frame.size.width, 30);
        UILabel *labelText = [[UILabel alloc] initWithFrame:labelFrame];
        [labelText setCenter:CGPointMake(content.frame.size.width/2, 30)];
        labelText.autoresizingMask = ~UIViewAutoresizingNone;
        labelText.text = textLabel;
        labelText.textColor = [UIColor whiteColor];
        labelText.backgroundColor = [UIColor clearColor];
        labelText.textAlignment = NSTextAlignmentCenter;
        labelText.font = [UIFont boldSystemFontOfSize:[UIFont labelFontSize]];
        [content addSubview:labelText];
    }
    
    [container addSubview:content];
    
    [loadingView addSubview:container];
    [self addSubview:loadingView];
    
    [self setLoadingView:loadingView];
    
    [UIView animateWithDuration:1.0 animations:^{
        [loadingView setAlpha:1.0f];
    }];
    
    return loadingView;
}


- (void) loadingStop
{
    UIView *loadingView = [self loadingView];
    
    NSNumber *totalLocks = [self loadingViewTotalLocks];
   
    //No more locks to check, just return;
    if ([totalLocks intValue] > 0) {
        
        NSNumber *newTotalLocks = [[NSNumber alloc] initWithInt:[totalLocks intValue] - 1];
        
        if ([newTotalLocks intValue] < 1 && [loadingView isMemberOfClass:[UIView class]]) {
            
            [UIView animateWithDuration:1.0 animations:^{
                
                [loadingView setAlpha:0.0f];
                
            } completion:^(BOOL finished) {
                
                [loadingView removeFromSuperview];
                
                UIViewLoadingViewBlock block = [self loadingViewBlock];
                
                if (block) {
                    block();
                }
                
            }];
            
        }
        
        [self setLoadingViewTotalLocks:newTotalLocks]; //store
    }
}


- (void) loadingHalt
{
    UIView *loadingView = [self loadingView];
    
    if ([loadingView isMemberOfClass:[UIView class]]) {
        
        [UIView animateWithDuration:1.0 animations:^{
            
            [loadingView setAlpha:0.0f];
            
        } completion:^(BOOL finished) {
            
            [loadingView removeFromSuperview];
            
        }];
    }
}



#pragma mark - Setting Methods

- (UIView *) loadingView
{
    return objc_getAssociatedObject(self, kLoadingView);
}

- (void) setLoadingView:(UIView *)withView
{
    objc_setAssociatedObject(self, kLoadingView, withView, OBJC_ASSOCIATION_RETAIN);
}


//Locks for the loadingView
- (NSNumber *) loadingViewTotalLocks
{
    return objc_getAssociatedObject(self, kLoadingViewTotalLocks);
}

- (void) setLoadingViewTotalLocks:(NSNumber *)withTotalLocks
{
    objc_setAssociatedObject(self, kLoadingViewTotalLocks, withTotalLocks, OBJC_ASSOCIATION_RETAIN);
}


//Background color of the loadingView
- (UIColor *) loadingViewBackgroundColor
{
    return objc_getAssociatedObject(self, kLoadingViewBackgroundColor);
}

- (void) setLoadingViewBackgroundColor:(UIColor *)withColor
{
    objc_setAssociatedObject(self, kLoadingViewBackgroundColor, withColor, OBJC_ASSOCIATION_RETAIN);
}


//Label of the loadingView
- (NSString*) loadingViewLabelText
{
    return objc_getAssociatedObject(self, kLoadingViewLabelText);
}

- (void) setLoadingViewLabelText:(NSString *)withText
{
    objc_setAssociatedObject(self, kLoadingViewLabelText, withText, OBJC_ASSOCIATION_RETAIN);
}


//Background color of the loadingView
- (CGFloat) loadingViewBorderRadius
{
    NSNumber *radius = (NSNumber*) objc_getAssociatedObject(self, kLoadingViewBorderRadius);
    return [radius floatValue];
}

- (void)setLoadingViewBorderRadius:(CGFloat)withRadius
{
    objc_setAssociatedObject(self, kLoadingViewBorderRadius, [NSNumber numberWithFloat:withRadius], OBJC_ASSOCIATION_RETAIN);
}


//Opacity color of the loadingView
- (CGFloat) loadingViewOpacity
{
    NSNumber *opacity = (NSNumber*) objc_getAssociatedObject(self, kLoadingViewOpacity);
    return [opacity floatValue];
}

- (void) setLoadingViewOpacity:(CGFloat)withOpacity
{
    objc_setAssociatedObject(self, kLoadingViewOpacity, [NSNumber numberWithFloat:withOpacity], OBJC_ASSOCIATION_RETAIN);
}


//Padding of the loadingView
- (CGFloat) loadingViewPadding
{
    NSNumber *padding = (NSNumber*) objc_getAssociatedObject(self, kLoadingViewPadding);
    return [padding floatValue];
}

- (void) setLoadingViewPadding:(CGFloat)withPadding
{
    objc_setAssociatedObject(self, kLoadingViewPadding, [NSNumber numberWithFloat:withPadding], OBJC_ASSOCIATION_RETAIN);
}


//set the target and selector
- (void) setLoadingViewBlock:(UIViewLoadingViewBlock) completionBlock
{

    objc_setAssociatedObject(self, kLoadingViewBlock, completionBlock , OBJC_ASSOCIATION_RETAIN);

}


//Padding of the loadingView
- (id) loadingViewBlock
{
    
    return objc_getAssociatedObject(self, kLoadingViewBlock);
}


/**
 aer 
 @author Magno Cardona & Luis Mancilla
 @version 0.1
 */
-(void) test {
    
}
@end
