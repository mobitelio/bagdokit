//
//  AppDelegate+Reachability.h
//  ErrazurizWineries
//
//  Created by Magno Cardona on 05-12-12.
//  Copyright (c) 2012 Francisco MuÃ±oz. All rights reserved.
//

#import "BKAppDelegate.h"
#import "Reachability.h"

@interface BKAppDelegate (Reachability)

@property (nonatomic, strong) NSTimer *timer;
@property (strong, nonatomic) Reachability *reachability;

#pragma mark - Reachability
- (void) initReachabilityWithHost: (NSString*) hostUrl;
- (void) haltReachability;
- (void) logReachability;
- (BOOL) isReachable;
- (BOOL) isReachableViaWiFi;
- (BOOL) isReachableViaWWAN;


@end
