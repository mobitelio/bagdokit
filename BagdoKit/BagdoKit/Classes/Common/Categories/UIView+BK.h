//
//  UIView+BK.h
//  BagdoKit
//
//  Created by Magno Cardona on 16-01-13.
//  Copyright (c) 2013 Magno Cardona. All rights reserved.
//

@interface UIView (BK)

/**
 * Shortcut for frame.origin.x.
 *
 * Sets frame.origin.x = left
 */
@property (nonatomic) CGFloat left;

/**
 * Shortcut for frame.origin.y
 *
 * Sets frame.origin.y = top
 */
@property (nonatomic) CGFloat top;

/**
 * Shortcut for frame.origin.x + frame.size.width
 *
 * Sets frame.origin.x = right - frame.size.width
 */
@property (nonatomic) CGFloat right;

/**
 * Shortcut for frame.origin.y + frame.size.height
 *
 * Sets frame.origin.y = bottom - frame.size.height
 */
@property (nonatomic) CGFloat bottom;

/**
 * Shortcut for frame.size.width
 *
 * Sets frame.size.width = width
 */
@property (nonatomic) CGFloat width;

/**
 * Shortcut for frame.size.height
 *
 * Sets frame.size.height = height
 */
@property (nonatomic) CGFloat height;

/**
 * Shortcut for center.x
 *
 * Sets center.x = centerX
 */
@property (nonatomic) CGFloat centerX;

/**
 * Shortcut for center.y
 *
 * Sets center.y = centerY
 */
@property (nonatomic) CGFloat centerY;


/**
 * Return the x coordinate on the screen.
 */
@property (nonatomic, readonly) CGFloat cordScreenX;

/**
 * Return the y coordinate on the screen.
 */
@property (nonatomic, readonly) CGFloat cordScreenY;

/**
 * Return the x coordinate on the screen, taking into account scroll views.
 */
@property (nonatomic, readonly) CGFloat screenViewX;

/**
 * Return the y coordinate on the screen, taking into account scroll views.
 */
@property (nonatomic, readonly) CGFloat screenViewY;

/**
 * Return the view frame on the screen, taking into account scroll views.
 */
@property (nonatomic, readonly) CGRect screenFrame;

/**
 * Shortcut for frame.origin
 */
@property (nonatomic) CGPoint origin;

/**
 * Shortcut for frame.size
 */
@property (nonatomic) CGSize size;

/**
 * Return the width in portrait or the height in landscape.
 */
@property (nonatomic, readonly) CGFloat orientationWidth;

/**
 * Return the height in portrait or the width in landscape.
 */
@property (nonatomic, readonly) CGFloat orientationHeight;

/**
 * Finds the first descendant view (including this view) that is a member of a particular class.
 */
- (UIView*)descendantOrSelfWithClass:(Class)cls;

/**
 * Finds the first ancestor view (including this view) that is a member of a particular class.
 */
- (UIView*)ancestorOrSelfWithClass:(Class)cls;

/**
 * Removes all subviews.
 */
- (void)removeAllSubviews;

/**
 * Calculates the offset of this view from another view in screen coordinates.
 *
 * otherView should be a parent view of this view.
 */
- (CGPoint)offsetFromView:(UIView*)otherView;

/**
 *  Gets the location of the view from the superview.subviews array
 */
-(int)getSubviewIndex;

/**
 *  Push this view to front of the subviews
 */
-(void)bringToFront;

/**
 *  Puts back the view of the subviews
 */
-(void)sendToBack;

/**
 *  Push one level to front
 */
-(void)bringOneLevelUp;

/**
 *  Push one level to back
 */
-(void)sendOneLevelDown;

/**
 *  checks if the view is on front
 */
-(BOOL)isInFront;

/**
 *  checks if the view is back
 */
-(BOOL)isAtBack;

/**
 *  changes this view with another view. Must belong to the same array of subviews
 */
-(void)swapDepthsWithView:(UIView*)swapView;

/**
 *  view to image does retina snapshot
 */
- (UIImage *)toImage;

/**
 *  to image with scale, original = 1
 */
- (UIImage *)toImageWithScale:(CGFloat)scale;

/**
 *  finds the controller the view belongs to
 */
- (UIViewController *)findViewController;

/**
 *  finds the navigationBar
 */
- (UINavigationBar *)findNavigationBar;
@end
 