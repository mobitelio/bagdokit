//
//  NSObject+BK.m
//  BagdoKit
//
//  Created by Magno Cardona on 23-01-13.
//  Copyright (c) 2013 Magno Cardona. All rights reserved.
//

#import "NSObject+BK.h"

@implementation NSObject (BK)


- (id) findElementInArrayOfDictionary:(NSArray*)array
                                       forKey:(NSString*)key
                                 withKeyValue:(NSString*)valueKey
                        andReturnThisKeyValue: (NSString*)keyReturn
{
    if (array) {
        for (NSDictionary *dic in array) {
            NSString *valueOfDIcKey = [dic objectForKey:key];
            
            if ((valueOfDIcKey != nil) &&
                (valueOfDIcKey != nil) &&
                [valueKey isEqualToString:valueOfDIcKey]) {
                return [dic objectForKey:keyReturn];
            }
        }
    }
    
    return nil;
}


- (int) findIndexInArrayOfDictionary:(NSArray*)array
                               forKey:(NSString*)key
                         withKeyValue:(NSString*)valueKey
{
    if (array) {
        int x = 0;
        for (NSDictionary *dic in array) {
            NSString *valueOfDIcKey = [dic objectForKey:key];
            
            if ((valueOfDIcKey != nil) &&
                (valueOfDIcKey != nil) &&
                [valueKey isEqualToString:valueOfDIcKey]) {
                return x;
            }
            x++;
        }
    }
    
    return -1;
}

- (BOOL)isRetina
{
    if ([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] &&
        ([UIScreen mainScreen].scale == 2.0)) return YES;
    else return NO;
}


- (BOOL)isIOS6
{
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 6) return true;
    else return false;
}

@end
