//
//  CALayer+LayerWithName.m
//  Whosapp
//
//  Created by Daniel Gutiérrez Torres on 30-06-12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CALayer+LayerWithName.h"


@implementation CALayer (LayerWithName)

-(CALayer *)layerWithName:(NSString *)name{
    for (CALayer *layer in [self sublayers]) {
        
        if ([[layer name] isEqualToString:name]) {
            return layer;
        }
    }
    return nil;
}

@end
