//
//  UIWebView+removeShadow.h
//  ErrazurizWineries
//
//  Created by Daniel Gutierrez on 9/21/12.
//  Copyright (c) 2012 Francisco MuÃ±oz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIWebView (removeShadow)
-(void)removeShadow;
@end
