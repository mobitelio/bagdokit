//
//  UIImageView+FX.m
//  Whosapp
//
//  Created by Daniel Gutiérrez Torres on 30-06-12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UIImageView+FX.h"


@implementation UIImageView (FX)

-(void)roundedCorners:(int)round{
    
    //If image has border
    if (self.layer.borderWidth > 0) {
        CALayer *image = [CALayer layer];
        image.name = @"imagen";
        image.frame = self.frame;
        image.contents = (id)[self.image CGImage];
        image.masksToBounds = YES;
        image.cornerRadius = round;
        [self.layer addSublayer:image];
        image.borderColor = self.layer.borderColor;
        image.borderWidth = self.layer.borderWidth;
        self.layer.borderWidth = 0; //Delete actual border
        self.image = Nil;
    }else{
        self.layer.masksToBounds = YES;
        self.layer.cornerRadius = round;
    }
    
    
}

//Note: Use this method before roundedCorners
-(void)bordersWithColor:(UIColor *)color withWidth:(int)width{        
    CALayer *image = [self.layer layerWithName:@"imagen"];
    
    //If image has rounded corners
    if(image){
        image.borderWidth = width;
        image.borderColor = [color CGColor];
    }else {
        self.layer.borderWidth = width;
        self.layer.borderColor = [color CGColor];
    }
    
}

-(void)shadowWithColor:(UIColor *)color withOpacity:(float)opacity withRadius:(int)radius withOffsetX:(int)offsetX withOffsetY:(int)offsetY{
    self.layer.shadowColor = [color CGColor];
    self.layer.shadowOffset = CGSizeMake(offsetX, offsetY);
    self.layer.shadowOpacity = opacity;
    self.layer.shadowRadius = radius;
    self.clipsToBounds = NO;
}

@end