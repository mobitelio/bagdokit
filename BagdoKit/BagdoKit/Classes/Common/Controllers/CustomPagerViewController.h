//
//  CustomPageViewController.h
//  PageViewController
//
//  Created by Tom Fewster on 11/01/2012.
//  Modified by Magno on 11/11/2012.
//

#import "PagerViewController.h"

@interface CustomPagerViewController : PagerViewController

- (void)reloadPages;

- (void)removePages;

@end
