//
//  CustomPageViewController.m
//  PageViewController
//
//  Created by Tom Fewster on 11/01/2012.
//  Modified by Magno on 11/11/2012.
//

#import "CustomPagerViewController.h"

@interface CustomPagerViewController ()

@end

@implementation CustomPagerViewController

- (void)viewDidLoad
{
	// Do any additional setup after loading the view, typically from a nib.
    [super viewDidLoad];

//	[self addChildViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"View1"]];
//	[self addChildViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"View2"]];
//	[self addChildViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"View3"]];

}


- (void)reloadPages {
    [super reloadPages];
}


- (void)removePages {
    [super removeAllPages];
}

@end
