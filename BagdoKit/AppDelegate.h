//
//  AppDelegate.h
//  BagdoKit
//
//  Created by Magno Cardona on 06-12-12.
//  Copyright (c) 2012 Magno Cardona. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
