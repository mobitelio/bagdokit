//
//  XPathQuery.h
//  FuelFinder
//
//  Created by Matt Gallagher on 4/08/08.
//  Copyright 2008 Matt Gallagher. All rights reserved.
//
//  Permission is given to use this source code file, free of charge, in any
//  project, commercial or otherwise, entirely at your risk, with the condition
//  that any redistribution (in part or whole) of source code must retain
//  this copyright and permission notice. Attribution in compiled projects is
//  appreciated but not required.
//
//  Requierements
//
//  1) In the Header Search Path add : /usr/include/libxml2 no recursive
//  2) In Other Linker Flag add: -lxml2
//  3) Add libxml2.dylib to project

   
//  Quick Example:
//
//  Get NSData from file:
//    #import "XPathQuery.h"
//
//    NSURL *urldata = [[NSBundle mainBundle] URLForResource:@"valorcuota" withExtension:@"nsdata"];
//    NSString*urldataPath = [urldata absoluteString]; //this is correct
//
//    NSData *resp = [NSData dataWithContentsOfURL:[NSURL URLWithString:urldataPath]];
//    NSArray *ra = PerformXMLXPathQuery(resp, @"//Fondo/Nombre");
//    NSLog(@"data: %@", ra);

///////////////////////////////////////////////////////////////////////////////////////////////
//  Example Usage PerformXMLXPathQuery:
//      NSArray *ra = PerformXMLXPathQuery(resp, @"//resultado/estadoLogin"); 
//      //NSLog(@"'estadoLogin' es: %@", [[ra objectAtIndex:0] objectForKey:@"nodeContent"]);
//
//
///////////////////////////////////////////////////////////////////////////////////////////////
//  Example Usage GetSingleNodeText:
//      NSDictionary *xPaths = [NSDictionary dictionaryWithObjectsAndKeys:
//                        @"//resultado/estadoLogin" ,@"estadoLogin",
//                        @"//estado", @"estado",
//                        nil];
//      NSMutableDictionary *responseData =  PerformXMLXPathQueries(resp, xPaths);
//
//      //each key, returns an array, in case of a single node, always use index 0.
//      //NSLog(@"Response estado text: %@", GetSingleNodeText([responseData objectForKey:@"estado"]));
//      //NSLog(@"Response estado text: %@", GetAtNodeText([responseData objectForKey:@"estado"],1));
//
//  
///////////////////////////////////////////////////////////////////////////////////////////////
//  Example Usage GetAtNodesFindSubNodeByName:
//      NSDictionary *xPaths = [NSDictionary dictionaryWithObjectsAndKeys:
//                              @"//resultado/mercado", @"mercados",     //estado del dia
//                              nil];
//
//      //keep the datas in individual array
//      NSMutableDictionary *responseData =  PerformXMLXPathQueries(resp, xPaths);
//
//      NSArray *mercados = [responseData objectForKey:@"mercados"];
//      //iterate all the mercados
//      for (int i=0; i<[mercados count]; i++) {
//          NSString *description = GetAtNodesFindSubNodeByName(data_mercados_hoy, i, @"description");
//          NSString *name = GetAtNodesFindSubNodeByName(data_mercados_hoy, i, @"name");
//      }




//Fast methods to extract nodeContent
NSArray *PerformHTMLXPathQuery(NSData *document, NSString *query);
NSArray *PerformXMLXPathQuery(NSData *document, NSString *query);

//Complete document extracts nodesContents
NSMutableDictionary *PerformXMLXPathQueries(NSData *document, NSDictionary *queries);

//Gets node as a string.
NSString *GetSingleNodeText(NSArray *data);
NSString *GetAtNodeText(NSArray *data, int index);
//Gest node name at index.
NSString *GetAtNodeName(NSArray *data, int index);

//Gets nodes as the array.
NSArray *GetAtNodes(NSArray *data, int index);


//Gets nodes as the array, then it looks for the first child nodes by name
NSString *GetAtNodesFindSubNodeByName(NSArray *data, int index, NSString *name);
//same as above but when subtags names are repeated, get them by index.
NSString *GetAtNodesFindSubNodeByIndex(NSArray *data, int index, int subIndex);

NSString *GetAtNodesFindSubNodeByNameCDATA(NSArray *data, int index, NSString *name);



